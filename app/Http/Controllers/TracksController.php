<?php

namespace App\Http\Controllers;

use App\Track;
use phpGPX\phpGPX;
use Illuminate\Http\Request;
use Illuminate\Filesystem\Filesystem;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;

class TracksController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        return view('tracks.index', [
            'tracks' => request()->user()->tracks
        ]);
    }


    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function create()
    {
        return view('tracks.create', [
            'tracks' => request()->user()->tracks
        ]);
    }


    /**
     * @param Request $req
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(Request $req)
    {
        $data = [
            'title' => $req->input('title'),
            'gps_file' => $req->file('gps_file'),
            'user_id' => $req->user()->id
        ];

        $validator = Validator::make($data, [
            'title' => 'required',
            'gps_file' => 'required|file|mimes:gpx,xml',
            'user_id' => 'required|integer'
        ]);

        if($validator->fails()) {
            return redirect(route('tracks.create'))
                ->withErrors($validator)
                ->withInput();
        }

        $data['gps_file']  = $this->storeTrackAsFile($data);

        $track = Track::create($data);

        return redirect(route('tracks.show', $track->id));
    }


    /**
     * @param $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function show($id)
    {
        $track = Track::findOrFail($id);

        $gpsData = $this->getGpsDataFromFile($track->gps_file);

        return view('tracks.show', [
            'tracks' => request()->user()->tracks,
            'track'  => $track,
            'gpsData' => $gpsData
        ]);
    }


    /**
     * @param $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function edit($id)
    {
        $track = Track::findOrFail($id);
        $tracks = request()->user()->tracks;

        return view('tracks.edit', [
            'track' => $track,
            'tracks' => $tracks
        ]);
    }

    /**
     * @param $id
     * @param Request $req
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update($id, Request $req)
    {
        $track = Track::findOrFail($id);

        $data = [
            'title' => $req->input('title'),
            'gps_file' => $req->file('gps_file'),
            'user_id' => $req->user()->id
        ];

        $rules = [
            'title' => 'required',
            'user_id' => 'required|integer'
        ];

        if(!$track->gps_file) {
            $rules['gps_file'] = 'required|file|mimes:gpx,xml';
        }

        $validator = Validator::make($data, $rules);

        if($validator->fails()) {
            return redirect(route('tracks.update'))
                ->withErrors($validator)
                ->withInput();
        }

        if($data['gps_file']) {
            $data['gps_file'] = $this->storeTrackAsFile($data);
        } else {
            unset($data['gps_file']);
        }

        $track->update($data);

        return redirect(route('tracks.show', $track->id));
    }

    /**
     * @param $data
     * @return mixed
     */
    private function storeTrackAsFile($data)
    {
        $filename = $data['gps_file']->getClientOriginalName();
        $extension = $data['gps_file']->extension();
        $path = $data['gps_file']->storeAs('/gps_data', date('Y_m_d_His') . '_' . md5($filename) . '.' . $extension);

        return $path;
    }

    /**
     * @param $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function delete($id)
    {
        $track = Track::findOrFail($id);

        if (Storage::exists($track->gps_file) && !Storage::delete($track->gps_file)) {
            abort(500);
        }

        $track->delete();

        return back();
    }

    /**
     * @param Filesystem $fileSystem
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function clearDb(Filesystem $fileSystem)
    {
        Track::query()->delete();
        $fileSystem->cleanDirectory(storage_path('app/gps_data'));

        return redirect(route('tracks.index'));
    }

    /**
     * @param $gpsFile
     * @return \phpGPX\Models\GpxFile
     */
    private function getGpsDataFromFile($gpsFile)
    {
        $gpx = new phpGPX();

        try {
            return $gpx->load(storage_path('app/').$gpsFile);
        } catch (\Exception $e) {
            abort(500);
        }
    }
}
