<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class GpsData extends Model
{
    protected $fillable = ['latitude', 'longitude','track_id'];
}
