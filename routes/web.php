<?php

Auth::routes();

// TRACKS
Route::get('/', 'TracksController@index')->name('tracks.index');
Route::get('/tracks', 'TracksController@index')->name('tracks.index');
Route::get('/tracks/create', 'TracksController@create')->name('tracks.create');
Route::get('/tracks/{id}/show', 'TracksController@show')->name('tracks.show');
Route::get('/tracks/{id}/edit', 'TracksController@edit')->name('tracks.edit');
Route::patch('/tracks/{id}/', 'TracksController@update')->name('tracks.update');
Route::post('/tracks', 'TracksController@store')->name('tracks.store');
Route::delete('/tracks/{id}', 'TracksController@delete')->name('tracks.delete');

Route::post('/clear-db', 'TracksController@clearDb')->name('clear-db');
