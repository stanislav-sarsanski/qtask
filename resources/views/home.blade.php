@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">

            <a href="{{route('tracks.create')}}" class="btn btn-primary mb-3">Add New Track</a>
            <p>You have added {{ count($tracks) }} tracks so far.</p>
            <table class="table">
                <tbody>
                    @foreach($tracks as $track)
                        <tr><th scope="row"><a href="{{ route('tracks.show', $track->id) }}">{{ $track->title }}</a></th></tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
</div>
@endsection
