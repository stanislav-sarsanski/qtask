@extends('layouts.app')

@section('content')

    <div class="container">
        <div class="row">
            <h2><i>{{ $track->title }}</i> preview</h2>
            <track-map :gps-data="{{ json_encode($gpsData) }}"></track-map>
        </div>
    </div>

@endsection
