@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-8">

                @if ($errors->any())
                    <div class="alert alert-danger">
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif

                <form method="post" action="{{ route('tracks.update', $track->id) }}" enctype="multipart/form-data">

                    @csrf
                    @method('PATCH')

                    <div class="form-group">
                        <label for="title">Title</label>
                        <input type="text" class="form-control" name="title" id="title" placeholder="Enter track title" value="{{ $track->title }}">
                    </div>

                    <div class="form-group">
                        <label for="gps_file">Your GPS data (please provide valid .gpx file)</label>
                        <input type="file" class="form-control-file"  name="gps_file" id="gps_file">
                    </div>

                    <button type="submit" class="btn btn-primary">Submit</button>
                </form>

            </div>
        </div>
    </div>
@endsection