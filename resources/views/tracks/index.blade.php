@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-8">

                <a href="{{route('tracks.create')}}" class="btn btn-primary mb-3">Add New Track</a>
                <p>You have added {{ count($tracks) }} {{ count($tracks) != 1 ? 'tracks' : 'track' }} so far.</p>
                <table class="table">
                    <tbody>
                    @foreach($tracks as $track)
                        <tr>
                            <th scope="row" style="display: flex">
                                <a href="{{ route('tracks.edit', $track->id) }}">{{ $track->title }}</a>
                                <div style="margin-left: auto; display: flex">

                                    <form method="post" action="{{ route('tracks.delete', $track->id) }}">
                                        @csrf
                                        @method('DELETE')
                                        <button class="btn btn-danger btn-sm" type="submit">DELETE</button>
                                    </form>
                                </div>
                            </th>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
@endsection
